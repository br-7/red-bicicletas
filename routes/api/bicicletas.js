var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerApi');

router.get('/', bicicletaController.bicileta_list);
router.post('/create', bicicletaController.bicileta_create);
router.delete('/delete', bicicletaController.bicileta_delete);
router.put('/update', bicicletaController.bicileta_update);

module.exports = router;