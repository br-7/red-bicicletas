var Bicicleta = function (id, color, modelo, ubicacion) {
    
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;

}

Bicicleta.prototype.toString = function () {
    
    return "id => " + this.id + " color => " + this.color + " modelo => " + this.modelo;

}

Bicicleta.findById = function(biciId) {
    var bici = Bicicleta.allBici.find(x => x.id == biciId);

    if(bici) {
        return bici;
    } else {
        throw new Error(` No existe una bicicleta con el id ${biciId}`);
    }
}

Bicicleta.removeById = function(biciId) {

    for (let i = 0; i < Bicicleta.allBici.length; i++) {
        
        if(Bicicleta.allBici[i].id == biciId) {

            Bicicleta.allBici.splice(i,1);
            break;

        }

    }
}

Bicicleta.allBici = [];
Bicicleta.add = function(bici) {

    Bicicleta.allBici.push(bici);

}


var a = new Bicicleta(1, "rojo", "H4000");

a.ubicacion = [14.5604925, -91.0173295];
Bicicleta.add(a);

var b = new Bicicleta(2, "rojo", "H5000");

b.ubicacion = [14.5604925, -91.1173595];
Bicicleta.add(b);


module.exports = Bicicleta;